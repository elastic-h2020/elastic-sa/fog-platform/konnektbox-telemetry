# KonnektBox Telemetry

This repo contains the python app to manage the KonnektBox telemetry.

## Install
This util requires Python3.
Install the required libraries by executing:
```
python3 -m pip install -r requirements.txt --user
``` 

## Configuration
The module can be configure using the following environment variables:
- MQTT_ID
- MQTT_HOST
- MQTT_PORT
- MQTT_PROTOCOL
- REST_PORT
- MONITOR_PERIOD (seconds)
- REPORT_PERIOD (seconds)
- CATEGORIES, e.g. {"Active":["CPU", "Disk", "Mem", "Net", "Tegra"]}
- PEER_LIST, e.g. {"eth0":["8.8.8.8", "8.8.4.4"], "wlan0":["8.8.8.8", "8.8.4.4"]}

## Usage
Execute the app using:
```
docker-compose up --build
```

## Dashboard
The included Compose provides additional components for Grafana visualization with a built-in dashboard for NVIDIA Jetson TX2 devices. In order to access the dashboard, go to http://device-IP:3000 using any web browser

## Acknowledgements
This work has been supported by the EU H2020 project ELASTIC, contract #825473.

