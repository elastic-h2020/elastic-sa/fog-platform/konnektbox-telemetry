# Standard imports
import time
import threading

# Modules
from modules.config import Config

# External packages
import psutil
import cpuinfo as cputil

# Config parameters and globals
config = Config()


class CPUInfo:
    def __init__(self):
        self.dict = {'usage': []}
        self.monitor_thread = threading.Thread(target=self.monitor, daemon=True)
        self.dict['model'] = cputil.get_cpu_info()['brand']
        self.dict['cores_physical'] = psutil.cpu_count(logical=False)
        self.dict['cores_logical'] = psutil.cpu_count()


    def start(self):
        self.monitor_thread.start()

    def get(self):
        """ Gets CPU info """

        # Get cpu usage in percentage:
        self.dict['usage'].append(psutil.cpu_percent())
        self.dict['freq'] = psutil.cpu_freq()

    def monitor(self):
        while True:
            tic = time.time()
            self.get()
            elap_time = time.time() - tic
            time.sleep(config.monitor_period-elap_time)

    def get_avg(self):
        """ Averages a number of CPU usage samples """

        try:
            self.dict['avg_usage'] = sum(self.dict['usage'])/len(self.dict['usage'])
        except ZeroDivisionError as e:
            config.logger.error("Math. error: {}".format(e))
        self.dict['usage'] = []
